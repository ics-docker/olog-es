FROM registry.esss.lu.se/ics-docker/maven:openjdk-11

ARG OLOG_VERSION=master

RUN git clone --depth 1 --branch $OLOG_VERSION https://github.com/Olog/olog-es.git /tmp/olog-es
RUN mvn -Pdeployable-jar -f /tmp/olog-es clean package
RUN echo $(ls -1 /tmp/olog-es/target/*.jar)

FROM openjdk:11

LABEL maintainer="anders.harrisson@ess.eu"

ADD http://artifactory.esss.lu.se/artifactory/certificates/esss-ca01-ca.cer /usr/local/share/ca-certificates/esss-ca01-ca.crt
RUN update-ca-certificates


COPY --from=0 /tmp/olog-es/target/service-olog-[0-9].[0-9].jar /opt/olog/app.jar
RUN useradd -ms /bin/bash olog
USER olog

CMD ["java", "-Djavax.net.ssl.trustStore=/usr/local/openjdk-11/lib/security/cacerts", "-jar", "/opt/olog/app.jar", "--spring.config.location=/opt/olog/olog-es.properties"]
